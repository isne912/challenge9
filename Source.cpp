#include <iostream>
#include "list.h"
#include "list.cpp"

int main()
{
     //Demonstrate My list classes
     List game;
     //Add element 1 to 5 to the list
     for(int i=1;i<=5;i++){
         game.tailPush(i);
     }
     //show the element
     game.display();
     //push 6 to the head
     cout<<"Add 6 to the head"<<endl;
     game.headPush(6);
     //show the element
     game.display();
     //push 7 to the tail
     cout<<"Add 7 to the tail"<<endl;
     game.tailPush(7);
     //show the element
     game.display();
     //remove head node
     cout<<"Remove head node"<<endl;
     game.headPop();
     //show the element
     game.display();
     //remove tail node
     cout<<"Remove tail node"<<endl;
     game.tailPop();
     //show the element
     game.display();
     //delete node->info number 3
     cout<<"delete node that has number 3"<<endl;
     game.deleteNode(3);
     //show the element
     game.display();
     //Add node number 5 to the tail
     cout<<"Add node number 5 to the tail"<<endl;
     game.tailPush(5);
     //show the element
     game.display();
     //delete node->info number 5
     cout<<"delete node that has number 5"<<endl;
     game.deleteNode(5);
     //show the element
     game.display();
     //check isInlist
     cout<<"Is 5 inlist ? "<<game.isInList(5)<<endl;
     cout<<"Is 1 inlist ? "<<game.isInList(1)<<endl;
}
