#include <iostream>
#include "list.h"

using namespace std;

List::~List() {//Destructor
	for(Node *p; !isEmpty(); ){//Check wether the list is empty or not
		p=head->next;
		delete head;
		head=p;
	}
}

void List::headPush(int x){//Add element to front of list
	Node *temp = new Node(x);//the node that gonna be added
	if(isEmpty()==true){
		head = temp;
		tail = temp;
	}else{
		Node *h = head;//tomporary node that point toward previous head node
		head = temp;
		head->next = h;
		h->previous = head;
	}
}

void List::tailPush(int x){//Add element to back of list
	Node *temp = new Node(x);//the node that gonna be added
	if(isEmpty()==true){
		head = temp;
		tail = temp;
	}else{
		Node *t = tail;//tomporary node that point toward previous tail node
		tail = temp;
		t->next = tail;
		tail->previous = t;
	}
}

int List::headPop(){//Remove and return element from front of list
	Node* h = head;
	head = head->next;
	head->previous = 0;
	int r = h->info;//to keep the removed node value then return it
	delete h;
	return r;
}

int List::tailPop(){//Remove and return element from tail of list
	Node* t = tail;
	tail=tail->previous;
	tail->next=0;
	int r = t->info;//to keep the removed node value then return it
	delete t;
	return r;
}

void List::deleteNode(int x){//Delete a particular value
	Node* current = head;
	bool has = false;
	while(1){
		//check wether it head or tail or between the list
		if(current->info == x){
			has = true;
		}
		if(has == true){
			if(current == tail){
				tail = current->previous;
				tail->next = 0;
			}
			else if(current == head){
				head = current->next;
				head->previous = 0;
			}
			else{
				current->previous->next = current->next;
				current->next->previous = current->previous;
			}
			Node* del = current;
			if(current->next == 0){
				break;
			}
			else{
				current = current->next;
			}
			delete del;//delete
			has = false;
		}
		else{
			if(current->next == 0){
				break;
			}
			else{
				current = current->next;
			}
		}
	}
}

bool List::isInList(int x){//Check if a particular value is in the list
	Node* current = head;
	bool has = false;
	while(1){
		if(current->info == x){
			has = true;
		}
		if(current->next != 0){
			current = current->next;
		}
		else{
			break;//this is tail node
		}
	}
	return has;
}

void List::display(){//display all value in the list
	Node* current = head;
	cout<<"List : ";
	while(1){
		cout<<current->info<<" ";
		if(current->next == 0){
			break;
		}
		else{
			current = current->next;
		}
	}
	cout<<endl;
}
